import java.util.Scanner;

public class Guess {
    private static Scanner scaner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Ваша задача угадать число.");
        for (int i=10; i<=30; i+=10) {
            playLevel(i);
        }
        System.out.println("Поздравляю! You are won!");
        scaner.close();
    }

    private static void playLevel(int range) {
        int number = (int) (Math.random() * range);
        while (true) {
            System.out.println("Угадайте число от 0 до " + range);
            int input_number = scaner.nextInt();
            if (input_number == number) {
                System.out.println("Вы ввели правильное число");
                break;
            } else if (input_number > number) {
                System.out.println("Загаданное число меньше");
            } else System.out.println("Загаданное число больше");
        }
    }
}
